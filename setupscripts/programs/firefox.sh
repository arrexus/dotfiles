#!/bin/bash

# Firefox browser
# https://launchpad.net/~mozillateam/+archive/ubuntu/firefox-next
sudo add-apt-repository ppa:mozillateam/ppa -y

sudo apt update
sudo apt install -y firefox-esr
