#!/bin/bash
if hash docker 2>/dev/null; then
    echo "Docker already installed."
else
    curl -fsSL https://get.docker.com -o get-docker.sh
    sh get-docker.sh
    sudo docker run hello-world
    rm get-docker.sh
fi

sudo groupadd docker
sudo usermod -aG docker $USER
