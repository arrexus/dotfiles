#!/bin/bash

# Python
pip3 install pipenv
pip3 install flake8
pip3 install autopep8
pip3 install black --user
