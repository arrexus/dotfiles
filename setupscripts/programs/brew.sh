#!/bin/bash

# Install Homebrew
yes | sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"

test -d ~/.linuxbrew && eval $(~/.linuxbrew/bin/brew shellenv)
test -d /home/linuxbrew/.linuxbrew && eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
test -r ~/.bash_profile && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.bash_profile
eval \$($(brew --prefix)/bin/brew shellenv)"

# Register changes without restarting
source ~/.profile

brew analytics off
echo "Installed Homebrew and turned off Google Analytics"
