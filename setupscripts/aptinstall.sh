#!/bin/bash

sudo apt update

function install {
  which $1 &> /dev/null

  if [ $? -ne 0 ]; then
    echo "Installing: ${1}..."
    sudo apt install -y $1
  else
    echo "Already installed: ${1}"
  fi
}

# Common build prereqs
install xz-utils
install build-essential
install make
install zlib
install apt-utils
# Basics
install chromium-browser
install curl
install git
install neovim
install python3-pip
install sakura
install tilix
# Font
install fontconfig
install fonts-firacode
# Image manipulation (required for neofetch image)
install imagemagick
# Screenshot
install flameshot
# Low-level config editor
install dconf-cli
