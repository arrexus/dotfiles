#!/bin/bash

./symlink.sh
./aptinstall.sh
./download.sh
./programs.sh
./desktop.sh

# Get all upgrades
sudo apt upgrade -y

# See our bash changes
source ~/.bashrc

echo "Finished!"
