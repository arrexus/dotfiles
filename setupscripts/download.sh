#!/bin/bash

# Download various things
# Run this after curl, git, etc. are installed

# Clone latest base16-shell for themes
echo "Cloning Base16-shell"
if [ -d ~/.config/base16-shell ]; then
    echo "Removing existing base16-shell directory"
    sudo rm -Rf ~/.config/base16-shell
fi
git clone https://github.com/chriskempson/base16-shell.git ~/.config/base16-shell

# Download and install neofetch
echo "Cloning neofetch into /opt"
if [ -d /opt/neofetch ]; then
    echo "Removing existing /opt/neofetch directory"
    sudo rm -Rf /opt/neofetch
fi
sudo git clone https://github.com/dylanaraps/neofetch /opt/neofetch
cd /opt/neofetch
sudo make install
