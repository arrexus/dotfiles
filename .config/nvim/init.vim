" Initial Setup (Automatic vim-plug)
if has('win32') || has('win64')
  let g:plugged_home = '~/AppData/Local/nvim/plugged'
else
  let g:plugged_home = '~/.config/nvim/plugged'
  if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
    silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
endif

call plug#begin(g:plugged_home)

  "Plug 'davidhalter/jedi-vim'
  " UI related
  Plug 'chriskempson/base16-vim'
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  " Visual Guide for Indents, not working
  Plug 'Yggdroot/indentLine'
  Plug 'dense-analysis/ale'
  " Markdown Preview
  Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
call plug#end()

filetype plugin indent on

" Configurations Part
" UI configuration
syntax on
syntax enable
" colorscheme
if filereadable(expand("~/.vimrc_background"))
  let base16colorspace=256
  source ~/.vimrc_background
endif
"let base16colorspace=256
"colorscheme base16-material
"set background=dark
" True Color Support if it's avaiable in terminal
if has("termguicolors")
    set termguicolors
endif
if has("gui_running")
  set guicursor=n-v-c-sm:block,i-ci-ve:block,r-cr-o:blocks
endif

"let g:airline_powerline_fonts = 1
" Ignore case, unless there is a capital letter
set ignorecase
set smartcase

" Line numbers
set number
set relativenumber

" Allow scrolling, and other mouse functions.
" Clicking on screen enables VISUAL mode, hold shift to prevent this.
" Press shift while selecting with the mouse or to right click.
set mouse=a
" Allow yanks to use system clipboard.
set clipboard=unnamedplus

" Formatters for :ALEFix
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'javascript': ['eslint'],'python': ['black'],
\}

" Neovim
" Escape to go to Normal Mode from neovim Terminal
:tnoremap <Esc> <C-\><C-n>

" MarkdownPreview port, ChromeOS can access this
let g:mkdp_port = '8787'
